package com.cadastrodepessoa;

import java.io.Serializable;
import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RemoteViews.RemoteView;
import android.widget.TextView;

import com.cadastrodepessoa.dominio.Pessoa;

public class TelaDeListagem extends ListActivity {

	public static final int TELA_ALTERAR = 1;
	public static final int TELA_DETALHES = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_de_listagem);

		@SuppressWarnings("unchecked")
		ArrayList<Pessoa> pessoas = (ArrayList<Pessoa>) getIntent()
				.getSerializableExtra("pessoas");

		ListaDePessoasAdapter adapter = new ListaDePessoasAdapter(this, pessoas);
		setListAdapter(adapter);

		registerForContextMenu(getListView());

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_tela_de_listagem, menu);

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		ListaDePessoasAdapter adapter = (ListaDePessoasAdapter) getListAdapter();
		Pessoa pessoa = adapter.getItem(position);

		Intent intent = new Intent(this, TelaDeDetalhes.class);
		intent.putExtra("pessoa", pessoa);
		startActivity(intent);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {

		if (intent != null && resultCode == TELA_ALTERAR) {
			Serializable s = intent.getSerializableExtra("pessoa");

			ListaDePessoasAdapter adapter = (ListaDePessoasAdapter) getListAdapter();

			if (s != null) {
				Pessoa pessoa = (Pessoa) getIntent().getSerializableExtra(
						"pessoa");
				int position = getIntent().getIntExtra("position", 0);

				if (position > 0) {
					adapter.remove(adapter.getItem(position));
					adapter.insert(pessoa, position);
				}
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();

		int position = info.position;

		ListaDePessoasAdapter adapter = (ListaDePessoasAdapter) getListAdapter();
		Pessoa pessoa = adapter.getItem(position);

		switch (item.getItemId()) {
		case R.id.btnAlterar:
			Intent intent = new Intent(this, TelaDeCadastro.class);
			intent.putExtra("pessoa", pessoa);
			intent.putExtra("position", position);
			startActivityForResult(intent, TELA_ALTERAR);
			break;

		case R.id.btnExcluir:
			// Implementar exclusão.
			break;

		}

		return true;
	}

	private class ListaDePessoasAdapter extends ArrayAdapter<Pessoa> {

		public ListaDePessoasAdapter(Context context, ArrayList<Pessoa> pessoas) {
			super(context, android.R.layout.simple_list_item_1, pessoas);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			TextView txtNome = null;
			if (convertView == null) {
				txtNome = new TextView(getContext());
			} else {
				txtNome = (TextView) convertView;
			}

			Pessoa pessoa = getItem(position);
			txtNome.setText(pessoa.getNome());

			return txtNome;
		}

	}

}
