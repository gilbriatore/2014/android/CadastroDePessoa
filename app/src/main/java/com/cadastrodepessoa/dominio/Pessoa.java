package com.cadastrodepessoa.dominio;

import java.io.Serializable;

public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String PESSOA = "pessoa";

	public static final String ID = "id";
	public static final String NOME = "nome";
	public static final String SOBRENOME = "sobrenome";

	private int id;
	private String nome;
	private String sobrenome;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

}
