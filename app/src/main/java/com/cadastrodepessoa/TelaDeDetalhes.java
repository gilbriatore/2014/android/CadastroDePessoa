package com.cadastrodepessoa;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.cadastrodepessoa.dominio.Pessoa;

public class TelaDeDetalhes extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_de_detalhes);

		TextView txtNome = (TextView) findViewById(R.id.txtNome);
		TextView txtSobrenome = (TextView) findViewById(R.id.txtSobrenome);

		Pessoa pessoa = (Pessoa) getIntent().getSerializableExtra("pessoa");
		txtNome.setText(pessoa.getNome());
		txtSobrenome.setText(pessoa.getSobrenome());

	}

}
